This is CNN-based text classifier. It use word2vec embeddings (and some stopwords filtering).

Installation
------------
You'll need :

- python3.5
- (optional) cuda toolkit. It'll must be faster than CPU on big datasets.
- gcc
- keras, theano, nltk, gensim, python-levenshtein python libraries. Use `pip install keras theano gensim nltk python-levenshtein`.

In windows installation can be more difficult.
I used:
- anaconda with python3.5
- cuda toolkit. Path to nvcc.exe added to PATH environment variable
- ms visual studio 2015 community with C++ support and windows 10 toolkit (you'll need corecrt.h). Added path to cl.exe to PATH and to corecrt.h - to INCLUDE
- `conda install mingw libpython` to install gcc and needed library
- pip install keras theano python-levenshtein gensim nltk
- added next to ".theanorc" :
```
[gcc]
cxxflags = -D_hypot=hypot
```

Both ways - you'll need to download nltk data.
Run python console and :
```
import nltk
nltk.download()
```

Examples
--------
You can see examples in demo.py (all stages - train/classification/instantiation from configuration)
  and weather_demo.py (just instantiation of pretrained classifier and classification).

As results we can see (for demo.py):
```
		C:\Users\user\pynlc-env\lib\site-packages\gensim\utils.py:840: UserWarning: detected Windows; aliasing chunkize to chunkize_serial
		  warnings.warn("detected Windows; aliasing chunkize to chunkize_serial")
		C:\Users\user\pynlc-env\lib\site-packages\gensim\utils.py:1015: UserWarning: Pattern library is not installed, lemmatization won't be available.
		  warnings.warn("Pattern library is not installed, lemmatization won't be available.")
		Using Theano backend.
		Using gpu device 0: GeForce GT 730 (CNMeM is disabled, cuDNN not available)
		Reuters:

		Train on 3000 samples, validate on 7000 samples
		Epoch 1/10
		  20/3000 [..............................] - ETA: 307s - loss: 0.6968 - acc: 0.5376
		 ....
		3000/3000 [==============================] - 640s - loss: 0.0018 - acc: 0.9996 - val_loss: 0.0019 - val_acc: 0.9996
		Epoch 8/10
		  20/3000 [..............................] - ETA: 323s - loss: 0.0012 - acc: 0.9994
		  ...
		3000/3000 [==============================] - 635s - loss: 0.0012 - acc: 0.9997 - val_loss: 9.2200e-04 - val_acc: 0.9998
		Epoch 9/10
		  20/3000 [..............................] - ETA: 315s - loss: 3.4387e-05 - acc: 1.0000
		...
		3000/3000 [==============================] - 879s - loss: 0.0012 - acc: 0.9997 - val_loss: 0.0016 - val_acc: 0.9995
		Epoch 10/10
		  20/3000 [..............................] - ETA: 327s - loss: 8.0144e-04 - acc: 0.9997
		...
		3000/3000 [==============================] - 655s - loss: 0.0012 - acc: 0.9997 - val_loss: 7.4761e-04 - val_acc: 0.9998
		reuters_test_labels.json 0.000151774189194
		reuters_car_test_labels.json 0.000151774189194

		Car intents:

		Train on 280 samples, validate on 120 samples
		Epoch 1/20
		 20/280 [=>............................] - ETA: 0s - loss: 0.6729 - acc: 0.5250
		 ...
		280/280 [==============================] - 0s - loss: 0.2914 - acc: 0.8980 - val_loss: 0.2282 - val_acc: 0.9375
		...
		Epoch 19/20
		 20/280 [=>............................] - ETA: 0s - loss: 0.0552 - acc: 0.9857
		 ...
		280/280 [==============================] - 0s - loss: 0.0464 - acc: 0.9842 - val_loss: 0.1647 - val_acc: 0.9494
		Epoch 20/20
		 20/280 [=>............................] - ETA: 0s - loss: 0.0636 - acc: 0.9714
		 ...
		280/280 [==============================] - 0s - loss: 0.0447 - acc: 0.9849 - val_loss: 0.1583 - val_acc: 0.9530
		cars_test_labels.json 0.0520754688092
		instantiated_cars_test_labels.json 0.0520754688092
		Weather:

		Train on 28 samples, validate on 12 samples
		Epoch 1/30
		20/28 [====================>.........] - ETA: 0s - loss: 0.6457 - acc: 0.6000
		...
		Epoch 29/30
		20/28 [====================>.........] - ETA: 0s - loss: 0.0021 - acc: 1.0000
		...
		28/28 [==============================] - 0s - loss: 0.0019 - acc: 1.0000 - val_loss: 0.1487 - val_acc: 0.9167
		Epoch 30/30
		...
		28/28 [==============================] - 0s - loss: 0.0018 - acc: 1.0000 - val_loss: 0.1517 - val_acc: 0.9167
		weather_test_labels.json 0.0136964029149
		instantiated_weather_test_labels.json 0.0136964029149
```
