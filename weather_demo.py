import json
from gensim.models import Word2Vec
from pynlc.test_data import word2vec
from pynlc import TextProcessor, TextClassifier


if __name__ == '__main__':
    text_processor = TextProcessor("english", [["turn", "on"], ["turn", "off"]],
                                   Word2Vec.load_word2vec_format(word2vec))
    with open("weather_trained.json", "r", encoding="utf-8") as classifier_data_source:
        classifier_data = json.load(classifier_data_source)
    classifier = TextClassifier(text_processor, **classifier_data)
    texts = [
        "Will it be windy or rainy at evening?",
        "How cold it'll be today?"
    ]
    predictions = classifier.predict(texts)
    for i in range(0, len(texts)):
        print(texts[i])
        print(predictions[i])
