from keras.models import Sequential, Model
from keras.layers import Convolution1D, MaxPooling1D, Dense, Dropout, Input, Flatten, Merge
from keras.optimizers import RMSprop
import numpy


class Classifier:
    def __init__(self, vector_count, vector_size, output_count, model=None, weights=None):
        """
        Classifier network
        :param vector_count: maximal input vector count
        :type vector_count: int
        :param vector_size: word vector size
        :type vector_size: int
        :param output_count: outputs count
        :type output_count: int
        :param model: pretrained model or None
        :type model: NoneType|dict
        :param weights: pretrained model weights
        :type weights: NoneType|dict
        """
        self.vector_count = vector_count
        self.vector_size = vector_size
        self.output_count = output_count
        if model is None:
            input = Input(shape=(self.vector_count, self.vector_size,))
            convs = []
            for filter_size in [1, 2]:
                conv = Convolution1D(nb_filter=150,
                                     filter_length=filter_size,
                                     border_mode='valid',
                                     activation='relu',
                                     subsample_length=1)(input)
                pool = MaxPooling1D(pool_length=2)(conv)
                flatten = Flatten()(pool)
                convs.append(flatten)
            out = Merge(mode='concat')(convs)
            graph = Model(input=input, output=out)
            self.model = Sequential()
            self.model.add(Dropout(0.25, input_shape=(self.vector_count, self.vector_size,)))
            self.model.add(graph)
            self.model.add(Dense(200))
            self.model.add(Dropout(0.25))
            self.model.add(Dense(output_count, activation='sigmoid'))
        else:
            self.model = Sequential.from_config(model)
        self.model.compile(RMSprop(lr=0.0005, epsilon=1e-10), 'binary_crossentropy', metrics=['accuracy'])
        if weights is not None:
            self.model.set_weights([numpy.array(layer_weights) for layer_weights in weights])

    def fit_matrixes(self, matrices):
        """
        Fit matrices to given vector count
        :param matrices: input
        :type matrices: numpy.ndarray
        :return: resized
        :rtype: numpy.ndarray
        """
        resized = [numpy.resize(matrix, (self.vector_count, self.vector_size,))
                   for matrix in matrices]
        return numpy.array(resized)

    def train(self, matrices, labels, epochs, verbose=False):
        """
        Train network on given examples
        :param matrices: input sentence matrices
        :type matrices: numpy.ndarray
        :param labels: input labels
        :type labels: numpy.ndarray
        :param epochs: epoch count
        :type epochs: int
        :param verbose: verbose train process?
        :type verbose: bool
        """
        features = self.fit_matrixes(matrices)
        self.model.fit(features, labels, nb_epoch=epochs, verbose=verbose, batch_size=20, validation_split=0.3)

    def predict(self, matrices):
        """
        Make prediction
        :param matrices: input sentence matrices
        :type matrices: numpy.ndarray
        :return: predicted classes
        :rtype: numpy.ndarray
        """
        features = self.fit_matrixes(matrices)
        return self.model.predict(features)

    @property
    def config(self):
        """
        Get configuration dictionary
        :return: config
        :rtype: dict
        """
        return {
            'vector_count': self.vector_count,
            'vector_size': self.vector_size,
            'output_count': self.output_count,
            'model': self.model.get_config(),
            'weights': [layer_weight.tolist() for layer_weight in self.model.get_weights()]
        }
