from .classifier import Classifier
from .text_processor import TextProcessor
import itertools
import numpy


class TextClassifier:
    def __init__(self, text_processor, backend=None, classes=None):
        """
        Initialize classifier
        :param text_processor: text processor
        :type text_processor: TextProcessor
        :param backend: pretrained network configuration or None
        :type backend: dict|NoneType
        :param classes: class names list (for pretrained classifier) or None
        :type classes: list[str]|NoneType
        """
        if backend is not None:
            self.backend = Classifier(**backend)
        else:
            self.backend = None
        self.text_processor = text_processor
        self.classes = classes

    @property
    def config(self):
        """
        Get configuration dictionary
        :return: config
        :rtype: dict
        """
        return {
            "backend": self.backend.config,
            "classes": self.classes
        }

    def train(self, texts, classes, epochs, verbose=False):
        """
        Train on given texts
        :param texts: texts
        :type texts: list[str]
        :param classes: class names (one list for one texts item)
        :type classes: list[list[str]]
        :param epochs: epochs count
        :type epochs: int
        :param verbose: verbose train process?
        :type verbose: bool
        """
        self.classes = list(set(itertools.chain(*classes)))
        self.classes.sort()
        matrixes = [self.text_processor.matrix(text)
                    for text in texts]
        vector_count = 2 * max([len(item) for item in matrixes])
        vector_size = matrixes[0].shape[1]
        labels = numpy.array([
                                 [int(class_name in text_classes) for class_name in self.classes]
                                 for text_classes in classes
                                 ])
        self.backend = Classifier(vector_count, vector_size, len(self.classes))
        self.backend.train(matrixes, labels, epochs, verbose)

    def predict(self, texts):
        """
        Predict classes
        :param texts: texts
        :type texts: list[str]
        :return: classification results (one per one texts item)
        :rtype: list[dict[str, float]]
        """
        matrixes = [self.text_processor.matrix(text)
                    for text in texts]
        labels = self.backend.predict(matrixes)
        result = []
        for row in labels:
            data = {}
            for i, class_name in enumerate(self.classes):
                data[class_name] = float(row[i])
            result.append(data)
        return result
